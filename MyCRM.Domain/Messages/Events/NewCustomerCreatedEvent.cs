﻿using System;
using MyCRM.Infrastructure.Messaging;

namespace MyCRM.Domain.Messages.Events
{
    public class NewCustomerCreatedEvent : DomainEvent
    {
        public Guid CustomerId { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }
    }
}