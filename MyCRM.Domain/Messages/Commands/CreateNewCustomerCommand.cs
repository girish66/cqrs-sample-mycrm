﻿using System;
using MyCRM.Infrastructure.Messaging;

namespace MyCRM.Domain.Messages.Commands
{
    public class CreateNewCustomerCommand : DomainCommand
    {
        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}