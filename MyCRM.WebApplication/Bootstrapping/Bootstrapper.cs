﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;

using MyCRM.Infrastructure;
using MyCRM.Infrastructure.Messaging;
using StructureMap;
using StructureMap.Graph;

namespace MyCRM.WebApplication.Bootstrapping {
  class BootStrapper {
    public static void ConfigureApplication() {
      ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
      RegisterDependencies();
      AutoRegisterCommandExecutors();
      AutoRegisterEventHandlers();
    }

    static void RegisterDependencies() {
      ObjectFactory.Configure(
        x => {
          x.AddRegistry(new MessageBusRegistry());
          x.AddRegistry(new StoringRegistry());
          x.Scan(RegisterMessagingContracts);
        });
    }

    static void RegisterMessagingContracts(
        IAssemblyScanner scanner) {
      var assemblies = new[] { "MyCRM.Domain", "MyCRM.ReadModel" };

      scanner.TheCallingAssembly();
      assemblies.ForEach(scanner.Assembly);
      scanner.WithDefaultConventions();
      scanner.AddAllTypesOf<DomainCommand>();
      scanner.AddAllTypesOf<DomainEvent>();
      scanner.ConnectImplementationsToTypesClosing(typeof(ICommandHandler<>));
      scanner.ConnectImplementationsToTypesClosing(typeof(IEventHandler<>));
    }

    static void AutoRegisterCommandExecutors() {
      var domainCommands = ObjectFactory.GetAllInstances<DomainCommand>();
      var commandMessageBus = ObjectFactory.GetInstance<IMessageBus>();

      domainCommands.ForEach(domainCommand => {
        var commandType = domainCommand.GetType();
        var closedCommandExecutorType = typeof(ICommandHandler<>).MakeGenericType(commandType);

        try {
          var closedDomainCommandExecutorInstance = ObjectFactory.GetInstance(closedCommandExecutorType);

          var actionType = Expression.GetActionType(commandType);
          var executeMethod = closedDomainCommandExecutorInstance.GetType().GetMethod("Execute", new Type[] { commandType });
          var executeActionDelegate = Delegate.CreateDelegate(actionType, closedDomainCommandExecutorInstance, executeMethod);

          var registerMethod = commandMessageBus.GetType().GetMethod("Register", BindingFlags.Instance | BindingFlags.Public);
          registerMethod = registerMethod.MakeGenericMethod(commandType);

          registerMethod.Invoke(commandMessageBus, new object[] { executeActionDelegate });
        } catch (Exception e) {
          Trace.WriteLine(e.Message);
        }
      });
    }
/*
<<<<<<< HEAD
    static void AutoRegisterEventHandlers() {
      var domainEvents = ObjectFactory.GetAllInstances<DomainEvent>();
      var eventMessageBus = ObjectFactory.GetInstance<IMessageBus>();

      domainEvents.ForEach(domainEvent => {
        var eventType = domainEvent.GetType();
        var closedDomainEventHandlerType = typeof(IEventHandler<>).MakeGenericType(eventType);

        try {
          var closedDomainEventHandlerInstance = ObjectFactory.GetInstance(closedDomainEventHandlerType);

          var actionType = Expression.GetActionType(eventType);
          var handleMethod = closedDomainEventHandlerInstance.GetType().GetMethod("Handle", new Type[] { eventType });
          var executeActionDelegate = Delegate.CreateDelegate(actionType, closedDomainEventHandlerInstance, handleMethod);

          var registerMethod = eventMessageBus.GetType().GetMethod("Register", BindingFlags.Instance | BindingFlags.Public);
          registerMethod = registerMethod.MakeGenericMethod(eventType);

          registerMethod.Invoke(eventMessageBus, new object[] { executeActionDelegate });
        } catch { }
      });
=======*/
        static void AutoRegisterEventHandlers()
        {
            var domainEvents = ServiceLocator.GetAllInstances(typeof(DomainEvent));

            domainEvents.Run(domainEvent =>
            {
                var eventType = domainEvent.GetType();
                var eventHandlerType = typeof(IEventHandler<>).MakeGenericType(eventType);

                try
                {
                    var eventHandlerInstances = ServiceLocator.GetAllInstances(eventHandlerType);

					eventHandlerInstances.ToList().Run(
						eventHandler => RegisterEventTypeToEventHandler(eventType, eventHandler));
                }
                catch { }
            });
        }

		static void RegisterEventTypeToEventHandler(Type eventType, object eventHandler)
		{
			var eventMessageBus = ServiceLocator.GetInstance<IMessageBus>();

			var actionType = Expression.GetActionType(eventType);
			var handleMethod = eventHandler.GetType().GetMethod("Handle", new Type[] { eventType });
			var executeActionDelegate = Delegate.CreateDelegate(actionType, eventHandler, handleMethod);

			var registerMethod = eventMessageBus.GetType().GetMethod("Register", BindingFlags.Instance | BindingFlags.Public);
			registerMethod = registerMethod.MakeGenericMethod(eventType);

			registerMethod.Invoke(eventMessageBus, new object[] { executeActionDelegate });
		}
//>>>>>>> remotes/janekf/cqrs-sample-mycrm/master
    }
  }
}