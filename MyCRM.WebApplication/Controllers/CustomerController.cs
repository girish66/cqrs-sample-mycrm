﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MyCRM.Domain.Messages;
using MyCRM.Domain.Messages.Commands;
using MyCRM.Infrastructure;
using MyCRM.Infrastructure.Messaging;
using MyCRM.Infrastructure.Repositories;
using MyCRM.ReadModel.ViewModels;
using MyCRM.WebApplication.Models;

namespace MyCRM.WebApplication.Controllers
{
    public class CustomerController : Controller
    {
      private readonly IMessageBus messagebus;
      private readonly IReadModelRepository readModelRepository;

      public CustomerController(IMessageBus messagebus, IReadModelRepository readModelRepository) {
        this.messagebus = messagebus;
        this.readModelRepository = readModelRepository;
      }

      public ActionResult Index()
        {
          var customers = readModelRepository.QueryAll<CustomerViewModel>();

            return View(customers.ToList());
        }

        public ActionResult CreateNewCustomer()
        { 
            return View(new CreateNewCustomerViewModel());
        }

        [HttpPost]
        public ActionResult CreateNewCustomer(CreateNewCustomerViewModel createNewCustomer)
        {
            if(ModelState.IsValid)
            {
                var customerId = Guid.NewGuid();

                createNewCustomer.CustomerId = customerId;

                var createNewCustomerCommand = new CreateNewCustomerCommand()
                    {
                        CustomerId = customerId,
                        AggregateId = customerId,
                        FirstName = createNewCustomer.FirstName,
                        LastName = createNewCustomer.LastName
                    };

                messagebus.Send(createNewCustomerCommand);
                // I'm realy sure that Version is 1 now
                // It is important for the next Step 'CretaeNewCustomerAddress' --> Goto the action, you will see the reason

                return RedirectToAction("CreateCustomerAddress", createNewCustomer);
            }

            return View(createNewCustomer);
        }


        public ActionResult CreateCustomerAddress(CreateNewCustomerViewModel customerCreaded)
        {
            return View(new CreateCustomerAddressViewModel()
                {
                    CustomerId = customerCreaded.CustomerId,
                    LastName = customerCreaded.LastName,
                    FirstName = customerCreaded.FirstName
                });
        }

        [HttpPost]
        public ActionResult CreateCustomerAddress(CreateCustomerAddressViewModel createNewCustomerAddress)
        {
            if(ModelState.IsValid)
            {
                var createCustomerAdressCommand = new CreateNewCustomerAddressCommand()
                    {
                        CuatomerId = createNewCustomerAddress.CustomerId,
                        Street = createNewCustomerAddress.Street,
                        Number = createNewCustomerAddress.Number,
                        City = createNewCustomerAddress.City,
                        PostalCode = createNewCustomerAddress.PostalCode
                    };

                messagebus.Send(createCustomerAdressCommand);

                return RedirectToAction("Index");
            }

            return View(createNewCustomerAddress);
        }


        public ActionResult ChangeCustomerAddress(CustomerViewModel customer)
        {
            return View(new ChangeCustomerAddressViewModel()
                {
                    City = customer.City,
                    CustomerId = customer.CustomerId,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Number = customer.Number,
                    PostalCode = customer.PostalCode,
                    Street = customer.Street,
                });
        }

        [HttpPost]
        public ActionResult ChangeCustomerAddress(ChangeCustomerAddressViewModel changeCustomerAddress)
        {
            if (ModelState.IsValid)
            {
                var changeCustomerAddressCommand = new ChangeCustomerAddressCommand()
                {
                    CuatomerId = changeCustomerAddress.CustomerId,
                    Street = changeCustomerAddress.Street,
                    Number = changeCustomerAddress.Number,
                    City = changeCustomerAddress.City,
                    PostalCode = changeCustomerAddress.PostalCode,
                };

                messagebus.Send(changeCustomerAddressCommand);

                return RedirectToAction("Index");
            }

            return View(changeCustomerAddress);
        }
    }
}
