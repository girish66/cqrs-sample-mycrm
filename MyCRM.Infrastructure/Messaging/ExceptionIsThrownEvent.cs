﻿using System;

namespace MyCRM.Infrastructure.Messaging
{
	public class ExceptionIsThrownEvent : DomainEvent
	{
		public Exception Exception { get; set; }
		public IMessage Message { get; set; }
	}
}
