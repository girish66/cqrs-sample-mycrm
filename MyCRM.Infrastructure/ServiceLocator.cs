﻿using StructureMap;

namespace MyCRM.Infrastructure {

  public class ServiceLocator {
    public static TService GetInstance<TService>() {
      return ObjectFactory.GetInstance<TService>();
    }
  }
}
