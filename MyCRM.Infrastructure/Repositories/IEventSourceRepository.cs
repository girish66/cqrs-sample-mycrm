﻿using System;

namespace MyCRM.Infrastructure.Repositories
{
    public interface IEventSourceRepository
    {
        void SaveEvents<T>(T aggregateId) where T : IAggregateRoot;
        T GetEventsBy<T>(Guid aggregateId) where T : IAggregateRoot, new();
    }
}